package logger

import (
	"log"
	"testing"
)

func TestLogger(t *testing.T) {
	l, err := New("logger_test.log")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()

	l.Debug("Debug test")
	l.Error("Error test")
	l.Info("Info test")
	l.Warning("Warning test")
}
