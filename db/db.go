package db

import (
	"errors"

	"github.com/boltdb/bolt"
)

// BucketPresences is bucket to store presence data
const BucketPresences = "presences"

// Database handles DB connection
type Database struct {
	Conn *bolt.DB
}

// Connect create new Database and connects to BoltDB with specified filename
func Connect(filename string) (*Database, error) {
	db := new(Database)
	var err error

	db.Conn, err = bolt.Open(filename, 0600, nil)
	if err != nil {
		return db, err
	}

	err = db.Conn.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(BucketPresences))
		if err != nil {
			return errors.New("Unable to create bucket")
		}
		return nil
	})

	return db, err
}

// Close closes opened BoltDB
func (db *Database) Close() {
	db.Conn.Close()
}
