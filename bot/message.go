package bot

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) hello(m *discordgo.MessageCreate) {
	b.Session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Hello %s!", m.Author.Username))
}
