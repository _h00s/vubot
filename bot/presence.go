package bot

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) seen(m *discordgo.MessageCreate) {
	username := strings.Split(m.Content, " ")[1]
	userID, _ := b.GetUserID(username)

	seen, err := b.DB.GetLastSeen(userID)
	if err != nil {
		b.Logger.Error("Error while getting DB last seen: " + err.Error())
		return
	}

	if string(seen) == "0" {
		b.Session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("User is currently online!"))
	} else if seen == nil {
		b.Session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("I have no seen data for this user"))
	} else {
		b.Session.ChannelMessageSend(m.ChannelID, fmt.Sprintf("User is offline since "+string(seen)))
	}
}

func (b *Bot) updatePresence(m *discordgo.PresenceUpdate) {
	b.Logger.Info(fmt.Sprintf("Presence changed for user %s [%s]", b.Users[m.Presence.User.ID], m.Presence.Status))
	err := b.DB.UpdatePresence(&m.Presence)
	if err != nil {
		b.Logger.Error(err.Error())
	}
}

func (b *Bot) updatePresences() {
	members, _ := b.Session.GuildMembers(b.Session.State.Guilds[0].ID, "", 1000)
	for _, member := range members {
		presence, err := b.Session.State.Presence(b.Session.State.Guilds[0].ID, member.User.ID)
		if err != nil {
			b.Logger.Error("Error while getting presence for member " + member.User.Username + ": " + err.Error())
			continue
		}
		err = b.DB.UpdatePresence(presence)
		if err != nil {
			b.Logger.Error("Error while saving presence: " + err.Error())
		}
	}
}
