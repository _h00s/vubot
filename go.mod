module vubot

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/boltdb/bolt v1.3.1
	github.com/bwmarrin/discordgo v0.20.2
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
)
